# laser_pointer_trigger

## What

* use to detect object 3d and UI on VR,MR,..

## Requirement

* UniRx
* .Net 4.x

## Install

```shell
yarn add laser_pointer_trigger
```

## Usage

* Detect object 3D:
    - On folder Prefabs drop LaserPointerHandle to the scene
    - Find component LaserLine and setting flied layer mask target want to detect object
    - On object want to detect set layer mask, add collider (box collider,...) and add script below

 ```csharp
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.EventSystems;
using UniRx;
 
public class Sample : UIBehaviour
{

    protected override void Start()
    {
        this.OnLaserTriggerEnterAsObservable()
            .Subscribe(message => Debug.Log(message.Point));

        this.OnLaserTriggerStayAsObservable()
            .Subscribe(message => Debug.Log(message.Point));

        this.OnLaserTriggerExitAsObservable()
            .Subscribe(message => Debug.Log(message.Point));

        this.OnLaserTriggerAsObservable()
            .Subscribe(message => Debug.Log(message.Point));
    }
}
```

* Detect UI 2D:
    - On Canvas add component LaserPoiterRayCaster
    - On Object EventSystem replace StandaloneInputModule by LaserPointerInputModule
    - In this can use every event (OnPointerDown, OnPointerUp, OnDrag, ...) by laser

```csharp
using UniRx.Triggers;
using UnityEngine;
using UnityEngine.EventSystems;
using UniRx;

public class TestUI : UIBehaviour
{
    protected override void Start()
    {
        this.OnPointerDownAsObservable()
            .Subscribe(_ => Debug.Log("pointer down"));

        this.OnDragAsObservable()
            .Subscribe(_ => Debug.Log("drag"));
    }
}
```

* Active trigger:
    - Methob ActiveTrigger() emit OnPointerDown() and OnLaserTriggerAsObservale()
    - Methob ExitTrigger() emit OnPointerUp()

- This template code:

```csharp
using LaserPointerTrigger;
using UniRx;
using UnityEngine;

public class TestLaserPointer : MonoBehaviour
{
    private void Start()
    {
        Observable.EveryUpdate()
            .Where(_ => Input.GetMouseButtonDown(0))
            .Subscribe(_ => GetComponent<LaserPointerHandler>().ActiveTrigger());

        Observable.EveryUpdate()
            .Where(_ => Input.GetMouseButtonUp(0))
            .Subscribe(_ => GetComponent<LaserPointerHandler>().ExitTrigger());
    }
}
```

## License

Copyright (c) 2019 thanhchp

Released under the MIT license, see [LICENSE.txt](LICENSE.txt)