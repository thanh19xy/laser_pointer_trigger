using LaserPointerTrigger;
using UniRx;
using UnityEngine;

public class TestLaserPointer : MonoBehaviour
{
    private void Start()
    {
        Observable.EveryUpdate()
            .Where(_ => Input.GetMouseButtonDown(0))
            .Subscribe(_ => GetComponent<LaserPointerHandler>().ActiveTrigger());

        Observable.EveryUpdate()
            .Where(_ => Input.GetMouseButtonUp(0))
            .Subscribe(_ => GetComponent<LaserPointerHandler>().ExitTrigger());
    }

    public void ActiveTrigger()
    {
        Debug.Log("Active");
    }
}