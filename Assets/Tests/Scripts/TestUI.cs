﻿using UniRx.Triggers;
using UnityEngine;
using UnityEngine.EventSystems;
using UniRx;

public class TestUI : UIBehaviour
{
    protected override void Start()
    {
        base.Start();

        this.OnDragAsObservable()
            .Subscribe(_ => Debug.Log("drag"));
        
    }
}