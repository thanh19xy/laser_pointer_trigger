﻿using UniRx.Triggers;
using UnityEngine;
using UnityEngine.EventSystems;
using UniRx;

public class Test : UIBehaviour
{
    [SerializeField] private MeshRenderer meshRenderer;
    [SerializeField] private Transform objectMarker;
    [SerializeField] private Transform objectMarker2;

    private MeshRenderer MeshRenderer => meshRenderer ? meshRenderer : meshRenderer = GetComponent<MeshRenderer>();

    private Transform ObjectMarker => objectMarker;

    private Transform ObjectMarker2 => objectMarker2;

    protected override void Start()
    {
        base.Start();

        this.OnLaserTriggerEnterAsObservable()
            .Subscribe(message =>
            {
                ObjectMarker.position = message.Point;
                MeshRenderer.material.color = message.ColorLaser;
                Debug.Log("Enter");
            });

        this.OnLaserTriggerStayAsObservable()
            .Subscribe(message =>
            {
                ObjectMarker2.position = message.Point;
                Debug.Log("Stay");
            });

        this.OnLaserTriggerExitAsObservable()
            .Subscribe(message =>
            {
                ObjectMarker.position = message.Point;
                MeshRenderer.material.color = Color.white;
                Debug.Log("Exit");
            }).AddTo(this);
    }
}