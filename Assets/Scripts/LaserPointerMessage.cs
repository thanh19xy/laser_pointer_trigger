using UnityEngine;

namespace LaserPointerTrigger
{
    public interface ILaserPointerMessage
    {
        /// <summary>
        ///   <para>The impact point in world space where the laser line the collider.</para>
        /// </summary>
        Vector3 Point { get; }

        Color ColorLaser { get; }
    }
    
    public class LaserPointerMessage : ILaserPointerMessage
    {
        public Vector3 Point { get; }

        public Color ColorLaser { get; }

        public LaserPointerMessage(Vector3 point, Color colorLaser)
        {
            Point = point;
            ColorLaser = colorLaser;
        }
    }
}