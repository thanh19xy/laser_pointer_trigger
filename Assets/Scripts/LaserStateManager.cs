using System;
using UniRx;
using UnityEngine;

namespace LaserPointerTrigger
{
    public class LaserStateManager : MonoBehaviour
    {
        private static LaserStateManager instance = null;

        private ReactiveProperty<LaserDetectMode> LaserDetectModeReactiveProperty { get; } =
            new ReactiveProperty<LaserDetectMode>(LaserPointerTrigger.LaserDetectMode.DetectObject);

        public static LaserStateManager Instance
        {
            get
            {
                if (instance == null)
                {
                    var singleton = new GameObject();
                    instance = singleton.AddComponent<LaserStateManager>();
                    singleton.name = "Singleton - " + typeof(LaserStateManager).ToString();
                }

                return instance;
            }
            private set { }
        }

        public LaserDetectMode LaserDetectMode
        {
            get { return LaserDetectModeReactiveProperty.Value; }
            set { LaserDetectModeReactiveProperty.Value = value; }
        }

        public IObservable<LaserDetectMode> OnChangeLaserDetectModeAsObservable()
        {
            return LaserDetectModeReactiveProperty.AsObservable();
        }
    }
}