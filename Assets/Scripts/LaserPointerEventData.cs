﻿using System.Text;
using UnityEngine.Assertions;

namespace UnityEngine.EventSystems
{
    public class LaserPointerEventData : PointerEventData
    {
        public LaserPointerEventData(EventSystem eventSystem) : base(eventSystem)
        {
        }

        public Ray worldSpaceRay;
        public Vector2 swipeStart;

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("<b>Position</b>: " + position);
            sb.AppendLine("<b>delta</b>: " + delta);
            sb.AppendLine("<b>eligibleForClick</b>: " + eligibleForClick);
            sb.AppendLine("<b>pointerEnter</b>: " + pointerEnter);
            sb.AppendLine("<b>pointerPress</b>: " + pointerPress);
            sb.AppendLine("<b>lastPointerPress</b>: " + lastPress);
            sb.AppendLine("<b>pointerDrag</b>: " + pointerDrag);
            sb.AppendLine("<b>worldSpaceRay</b>: " + worldSpaceRay);
            sb.AppendLine("<b>swipeStart</b>: " + swipeStart);
            sb.AppendLine("<b>Use Drag Threshold</b>: " + useDragThreshold);
            return sb.ToString();
        }
    }

    public static class PointerEventDataExtension
    {
        public static bool IsLaserPointer(this PointerEventData pointerEventData)
        {
            return (pointerEventData is LaserPointerEventData);
        }

        public static Ray GetRay(this PointerEventData pointerEventData)
        {
            var laserPointerEventData = pointerEventData as LaserPointerEventData;
            Assert.IsNotNull(laserPointerEventData);

            return laserPointerEventData.worldSpaceRay;
        }

        public static Vector2 GetSwipeStart(this PointerEventData pointerEventData)
        {
            var laserPointerEventData = pointerEventData as LaserPointerEventData;
            Assert.IsNotNull(laserPointerEventData);

            return laserPointerEventData.swipeStart;
        }

        public static void SetSwipeStart(this PointerEventData pointerEventData, Vector2 start)
        {
            var laserPointerEventData = pointerEventData as LaserPointerEventData;
            Assert.IsNotNull(laserPointerEventData);

            laserPointerEventData.swipeStart = start;
        }
    }
}