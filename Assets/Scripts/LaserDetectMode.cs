namespace LaserPointerTrigger
{
    public enum LaserDetectMode
    {
        None,
        DetectUI2D,
        DetectObject,
    }
}