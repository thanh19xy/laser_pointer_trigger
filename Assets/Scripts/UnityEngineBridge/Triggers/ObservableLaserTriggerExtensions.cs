using System;
using UnityEngine.EventSystems;
using UnityEngine;
using LaserPointerTrigger;

namespace UniRx.Triggers
{
    public static partial class ObservableLaserTriggerExtensions
    {
        public static IObservable<ILaserPointerMessage> OnLaserTriggerEnterAsObservable(this UIBehaviour component)
        {
            if (component == null || component.gameObject == null) return Observable.Empty<ILaserPointerMessage>();
            return GetOrAddComponent<ObservableLaserTrigger>(component.gameObject).OnLaserTriggerEnterAsObservable();
        }

        public static IObservable<ILaserPointerMessage> OnLaserTriggerExitAsObservable(this UIBehaviour component)
        {
            if (component == null || component.gameObject == null) return Observable.Empty<ILaserPointerMessage>();
            return GetOrAddComponent<ObservableLaserTrigger>(component.gameObject).OnLaserTriggerExitAsObservable();
        }

        public static IObservable<ILaserPointerMessage> OnLaserTriggerStayAsObservable(this UIBehaviour component)
        {
            if (component == null || component.gameObject == null) return Observable.Empty<ILaserPointerMessage>();
            return GetOrAddComponent<ObservableLaserTrigger>(component.gameObject).OnLaserTriggerStayAsObservable();
        }

        public static IObservable<ILaserPointerMessage> OnLaserTriggerAsObservable(this UIBehaviour component)
        {
            if (component == null || component.gameObject == null) return Observable.Empty<ILaserPointerMessage>();
            return GetOrAddComponent<ObservableLaserTrigger>(component.gameObject).OnLaserTriggerAsObservable();
        }

        private static T GetOrAddComponent<T>(GameObject gameObject)
            where T : Component
        {
            var component = gameObject.GetComponent<T>();
            if (component == null)
            {
                component = gameObject.AddComponent<T>();
            }

            return component;
        }
    }
}