﻿using System;
using LaserPointerTrigger;
using UnityEngine;

namespace UniRx.Triggers
{
    [DisallowMultipleComponent]
    public class ObservableLaserTrigger : ObservableTriggerBase
    {
        private Subject<ILaserPointerMessage> OnLaserTriggerEnterSubject { get; set; }

        private Subject<ILaserPointerMessage> OnLaserTriggerExitSubject { get; set; }

        private Subject<ILaserPointerMessage> OnLaserTriggerStaySubject { get; set; }

        private Subject<ILaserPointerMessage> OnLaserTriggerSubject { get; set; }

        public IObservable<ILaserPointerMessage> OnLaserTriggerEnterAsObservable() =>
            OnLaserTriggerEnterSubject ?? (OnLaserTriggerEnterSubject = new Subject<ILaserPointerMessage>());

        public IObservable<ILaserPointerMessage> OnLaserTriggerExitAsObservable() =>
            OnLaserTriggerExitSubject ?? (OnLaserTriggerExitSubject = new Subject<ILaserPointerMessage>());

        public IObservable<ILaserPointerMessage> OnLaserTriggerStayAsObservable() =>
            OnLaserTriggerStaySubject ?? (OnLaserTriggerStaySubject = new Subject<ILaserPointerMessage>());

        public IObservable<ILaserPointerMessage> OnLaserTriggerAsObservable() =>
            OnLaserTriggerSubject ?? (OnLaserTriggerSubject = new Subject<ILaserPointerMessage>());

        public void OnLaserTriggerEnter(ILaserPointerMessage message)
        {
            OnLaserTriggerEnterSubject?.OnNext(message);
        }

        public void OnLaserTriggerExit(ILaserPointerMessage message)
        {
            OnLaserTriggerExitSubject?.OnNext(message);
        }

        public void OnLaserTriggerStay(ILaserPointerMessage message)
        {
            OnLaserTriggerStaySubject?.OnNext(message);
        }

        public void OnLaserTrigger(ILaserPointerMessage message)
        {
            OnLaserTriggerSubject?.OnNext(message);
        }

        protected override void RaiseOnCompletedOnDestroy()
        {
            OnLaserTriggerEnterSubject?.OnCompleted();
            OnLaserTriggerExitSubject?.OnCompleted();
            OnLaserTriggerStaySubject?.OnCompleted();
            OnLaserTriggerSubject?.OnCompleted();
        }
    }
}