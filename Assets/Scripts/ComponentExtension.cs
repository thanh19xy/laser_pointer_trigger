using UnityEngine;

namespace LaserPointerTrigger
{
    public static class ComponentExtension
    {
        public static T GetOrAddComponent<T>(this Component gameObject)
            where T : Component
        {
            var component = gameObject.GetComponent<T>();
            if (component == null)
            {
                component = gameObject.gameObject.AddComponent<T>();
            }

            return component;
        }

        public static T GetOrCreateChildComponent<T>(this Component gameObject) where T : Component
        {
            var component = gameObject.GetComponentInChildren<T>();
            if (component == null)
            {
                component = new GameObject($"{typeof(T)}").AddComponent<T>();
                component.transform.parent = gameObject.transform;
                component.transform.localPosition = Vector3.zero;
            }

            return component;
        }
    }
}