using System;
using UniRx;
using UnityEngine;

namespace LaserPointerTrigger
{
    public class LaserPointerController : MonoBehaviour
    {
        [SerializeField] private LaserLine laserLine;

        private LaserLine LaserLine =>
            laserLine ? laserLine : laserLine = this.GetOrCreateChildComponent<LaserLine>();

        private Subject<Unit> ActiveTriggerSubject { get; } = new Subject<Unit>();
        
        private Subject<Unit> ExitTriggerSubject { get; } = new Subject<Unit>();

        public IObservable<RaycastHit> OnChangeGameObjectDetectAsObservable()
        {
            return LaserLine.OnChangeGameObjectDetectAsObservable();
        }

        public IObservable<ILaserPointerMessage> OnChangePointColliderAsObservable()
        {
            return LaserLine.OnChangePointColliderAsObservable();
        }

        public IObservable<ILaserPointerMessage> OnLastPointColliderAsObservable()
        {
            return LaserLine.OnLastPointColliderAsObservable();
        }

        public IObservable<Vector3> OnChangeVectorDirectionAsObservable()
        {
            return LaserLine.OnChangeVectorDirectionAsObservable();
        }

        public IObservable<Ray> OnChangeRayDirectionAsObservable()
        {
            return LaserLine.OnChangeRayDirectionAsObservable();
        }

        public Ray GetRayCurrent()
        {
            return LaserLine.GetRayCurrent();
        }

        public void ActiveTrigger()
        {
            ActiveTriggerSubject.OnNext(Unit.Default);
        }

        public IObservable<Unit> OnActiveTriggerAsObservable()
        {
            return ActiveTriggerSubject.AsObservable();
        }

        public void ExitTrigger()
        {
            ExitTriggerSubject.OnNext(Unit.Default);
        }

        public IObservable<Unit> OnExitTriggerAsObservable()
        {
            return ExitTriggerSubject.AsObservable();
        }
    }
}