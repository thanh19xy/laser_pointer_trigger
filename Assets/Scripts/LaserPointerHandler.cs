﻿using System;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

namespace LaserPointerTrigger
{
    public class LaserPointerHandler : MonoBehaviour
    {
        [SerializeField] private LaserPointerController laserPointerController;

        private LaserPointerController LaserPointerController => laserPointerController
            ? laserPointerController
            : laserPointerController = this.GetOrAddComponent<LaserPointerController>();

        private ObservableLaserTrigger ObservableLaserTriggerCurrent { get; set; } = null;

        private Subject<bool> ActiveTriggerSubject { get; } = new Subject<bool>();

        private ILaserPointerMessage LaserPointerMessageCurrent { get; set; }

        private bool IsActiveTrigger { get; set; } = false;

        private bool IsChangeGameObject { get; set; }

        protected virtual void Start()
        {
            LaserPointerController.OnChangeGameObjectDetectAsObservable()
                .Do(_ => IsChangeGameObject = true)
                .Subscribe(hit => ObservableLaserTriggerCurrent = hit.collider.GetComponent<ObservableLaserTrigger>());

            LaserPointerController.OnChangePointColliderAsObservable()
                .Where(_ => IsChangeGameObject)
                .Do(_ => IsChangeGameObject = false)
                .Subscribe(TriggerEnter);

            LaserPointerController.OnChangePointColliderAsObservable()
                .Subscribe(TriggerStay);

            LaserPointerController.OnLastPointColliderAsObservable()
                .Subscribe(TriggerExit);

            this.OnActiveTriggerAsObservable()
                .Subscribe(_ => TriggerActive(LaserPointerMessageCurrent));

            this.OnActiveTriggerAsObservable()
                .Subscribe(_ => LaserPointerController.ActiveTrigger());

            this.OnExitTriggerAsObservable()
                .Subscribe(_ => LaserPointerController.ExitTrigger());
        }

        private void TriggerEnter(ILaserPointerMessage message)
        {
            LaserPointerMessageCurrent = message;
            ObservableLaserTriggerCurrent?.OnLaserTriggerEnter(message);
        }

        private void TriggerExit(ILaserPointerMessage message)
        {
            ObservableLaserTriggerCurrent?.OnLaserTriggerExit(message);
            ObservableLaserTriggerCurrent = null;
        }

        private void TriggerStay(ILaserPointerMessage message)
        {
            LaserPointerMessageCurrent = message;
            ObservableLaserTriggerCurrent?.OnLaserTriggerStay(message);
        }

        private void TriggerActive(ILaserPointerMessage message)
        {
            ObservableLaserTriggerCurrent?.OnLaserTrigger(message);
        }

        public virtual void ActiveTrigger()
        {
            if (IsActiveTrigger) return;
            IsActiveTrigger = true;
            ActiveTriggerSubject.OnNext(true);
        }

        public IObservable<Unit> OnActiveTriggerAsObservable()
        {
            return ActiveTriggerSubject.Where(x => x).AsUnitObservable();
        }

        public virtual void ExitTrigger()
        {
            if (!IsActiveTrigger) return;
            IsActiveTrigger = false;
            ActiveTriggerSubject.OnNext(false);
        }

        public IObservable<Unit> OnExitTriggerAsObservable()
        {
            return ActiveTriggerSubject.Where(x => !x).AsUnitObservable();
        }
    }
}