﻿using System;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

namespace LaserPointerTrigger
{
    [RequireComponent(typeof(LineRenderer))]
    public class LaserLine : MonoBehaviour
    {
        private RaycastHit _hit;
        [SerializeField] private SettingLaserLine _settings;
        [SerializeField] private LineRenderer _lineRenderer;
        [SerializeField] private Transform _objectOrigin;
        [SerializeField] private Transform _objectDirection;

        private SettingLaserLine Settings => _settings;

        private LineRenderer LineRenderer =>
            _lineRenderer ? _lineRenderer : _lineRenderer = GetComponent<LineRenderer>();

        private Transform ObjectOrigin => _objectOrigin ? _objectOrigin : _objectOrigin = transform;

        private Transform ObjectDirection => _objectDirection;

        private GameObject GameObjectDetectCurrent { get; set; }

        private Vector3 PointColliderCurrent { get; set; } = Vector3.zero;

        private Subject<ILaserPointerMessage> OnLastPointColliderSubject { get; } = new Subject<ILaserPointerMessage>();

        private void OnValidate()
        {
            Initialize();
        }

        private void OnEnable()
        {
            LaserStateManager.Instance.LaserDetectMode = LaserDetectMode.DetectObject;
        }

        private void OnDisable()
        {
            LaserStateManager.Instance.LaserDetectMode = LaserDetectMode.None;
        }

        private void Initialize()
        {
            LineRenderer.material = new Material(Shader.Find("Particles/Additive"));
            LineRenderer.startColor = Settings.ColorLaserStart;
            LineRenderer.endColor = Settings.ColorLaserEnd;
            LineRenderer.SetPosition(0, transform.position);
            LineRenderer.SetPosition(1,
                ((ObjectDirection) ? GetDirection() : transform.forward) * Settings.MaxDistance);
            LineRenderer.startWidth = Settings.Width;
            LineRenderer.endWidth = Settings.Width;
        }

        private void Start()
        {
            this.OnChangePointColliderAsObservable()
                .Subscribe(message =>
                {
                    PointColliderCurrent = message.Point;
                    LineRenderer.SetPositions(new[] {ObjectOrigin.position, message.Point});
                });

            //Not detected object
            this.IsDetectObjectAsObservable()
                .Where(x => !x)
                .Subscribe(_ =>
                {
                    LineRenderer.SetPositions(new[]
                    {
                        ObjectOrigin.position,
                        ObjectDirection.transform.position + Settings.MaxDistance * GetDirection()
                    });
                });

            this.IsDetectObjectAsObservable()
                .DistinctUntilChanged()
                .Where(x => !x)
                .Subscribe(_ => GameObjectDetectCurrent = null);
        }

        public IObservable<bool> IsDetectObjectAsObservable()
        {
            return this.FixedUpdateAsObservable()
                .Select(_ => GetDirection())
                .Select(IsDetectedGameObject)
                .Merge(this.OnDisableAsObservable().Select(_ => false));
        }

        public IObservable<RaycastHit> OnChangeGameObjectDetectAsObservable()
        {
            return IsDetectObjectAsObservable()
                .Where(x => x)
                .Select(_ => _hit.collider.gameObject)
                .Where(obj => obj != GameObjectDetectCurrent)
                .Do(DoOnChangeObjectDetect)
                .Select(_ => _hit);
        }

        public IObservable<ILaserPointerMessage> OnChangePointColliderAsObservable()
        {
            return IsDetectObjectAsObservable()
                .Where(x => x)
                .Select(_ => _hit.point)
                .DistinctUntilChanged()
                .Select(point => new LaserPointerMessage(point, Settings.ColorLaserEnd));
        }

        public IObservable<ILaserPointerMessage> OnLastPointColliderAsObservable()
        {
            var onNotDetectObjectStream = IsDetectObjectAsObservable()
                .Where(isDetected => !isDetected)
                .Select(_ => new LaserPointerMessage(PointColliderCurrent, Settings.ColorLaserEnd));
            return onNotDetectObjectStream
                .Merge(OnLastPointColliderSubject);
        }

        public IObservable<Vector3> OnChangeVectorDirectionAsObservable()
        {
            return this.UpdateAsObservable()
                .Select(_ => (ObjectDirection) ? GetDirection() : transform.forward)
                .DistinctUntilChanged();
        }

        public IObservable<Ray> OnChangeRayDirectionAsObservable()
        {
            return OnChangeVectorDirectionAsObservable()
                .Select(direction => new Ray(transform.position, direction));
        }

        public Ray GetRayCurrent()
        {
            return new Ray(transform.position, (ObjectDirection) ? GetDirection() : transform.forward);
        }

        private Vector3 GetDirection()
        {
            return (ObjectDirection.transform.position - ObjectOrigin.transform.position).normalized;
        }

        private bool IsDetectedGameObject(Vector3 direction)
        {
            if (LaserStateManager.Instance.LaserDetectMode != LaserDetectMode.DetectObject)
                return false;

            return Physics.Raycast(ObjectOrigin.transform.position, direction, out _hit, Settings.MaxDistance,
                Settings.LayerMaskTarget);
        }

        private void DoOnChangeObjectDetect(GameObject objectDetect)
        {
            OnLastPointColliderSubject.OnNext(new LaserPointerMessage(PointColliderCurrent,
                Settings.ColorLaserEnd));
            GameObjectDetectCurrent = objectDetect;
        }

        [Serializable]
        private class SettingLaserLine
        {
            [SerializeField] private LayerMask _layerMaskTarget;
            [SerializeField] private Color _colorLaserStart = Color.red;
            [SerializeField] private Color _colorLaserEnd = Color.red;
            [SerializeField] private float _maxDistance = 1000;
            [SerializeField] [Range(0, 0.1f)] private float _width = 0.05f;

            public LayerMask LayerMaskTarget => _layerMaskTarget;

            public Color ColorLaserStart => _colorLaserStart;

            public Color ColorLaserEnd => _colorLaserEnd;

            public float MaxDistance => _maxDistance;

            public float Width => _width;
        }
    }
}